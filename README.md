# mixup

Mixup is a simple utility for obfuscating integers.  It uses the
DES3 encryption with an application-supplied key to transform
64 bit integers into another integer.  Since this is simple
block encryption, the mapping is a bijection.  In other words,
if you feed unique integers in, you get unique integers out.

The purpose is to obfuscate serial numbers.  A source of sequential
(or nearly so, e.g., using a block allocation scheme to distribute
chunks of numbers) serial numbers can be fed in, and via obfuscation,
an entity observing the stream of assigned serials will not be able to
tell how many allocations have taken place, thus avoiding the exposure
of traffic rate data.

## Notation

This package operates on integers (`uint64`), but once common practice
is to encode the numbers using base36 which is fairly compact and
still encourages the higher layers to treat them as strings which can
be useful in case the notation needs to be changed in the future.

```
n := mixup.New("secret").Uint64(123)
fmt.Printf("assigned serial <%s>\n", strconv.FormatUint(n, 36))
```

## Command line tool

This package includes a command-line tool to help the developer
recover original numbers.

The tool defaults to decimal; use `-a` to specify base 36 and `-x` to
specify base 16.  The default is to unmix (decrypt); specify the `-m`
flag for mixing (encryption).

```
$ go install bitbucket.org/dkolbly/mixup/cmd/unmix
$ unmix -m -a foobar 1 2 3
1 -> 3ntsr9nwqtuw
2 -> 1alqc8d1qs4w1
3 -> 38tyqu2dtsqqd
$ unmix -a foobar 3ntsr9nwqtuw 1alqc8d1qs4w1
1 <- 3ntsr9nwqtuw
2 <- 1alqc8d1qs4w1
```


## Usage

```
package main

import (
	"fmt"

	"bitbucket.org/dkolbly/mixup"
)

func main() {
	mix := mixup.New("foobar")
	var x uint64 = 123   // start with a number
	y := mix.Uint64(x)   // mix it up
	z := mix.UnUint64(y) // and unmix back to the original

	fmt.Printf("%d --(mix)--> %d --(unmix)--> %d\n", x, y, z)
}
```
