package varmix

import (
	"crypto/sha256"
	"math/bits"
	"math/rand"
)

const preXorRounds = 7
const postXorRounds = 9

type Mixer struct {
	key [256]uint8
	xor uint64
}

func New(secret string) *Mixer {
	h := sha256.New()
	h.Write([]byte(secret))
	hashed := h.Sum(nil)

	m := &Mixer{}
	for i := 0; i < 256; i++ {
		m.key[i] = uint8(i)
	}
	seed := int64(hashed[0]) +
		(int64(hashed[1]) << 8) +
		(int64(hashed[2]) << 16) +
		(int64(hashed[3]) << 24) +
		(int64(hashed[4]) << 32) +
		(int64(hashed[5]) << 40) +
		(int64(hashed[6]) << 48) +
		(int64(hashed[7]) << 56)
	m.xor = uint64(hashed[8]) +
		(uint64(hashed[9]) << 8) +
		(uint64(hashed[10]) << 16) +
		(uint64(hashed[11]) << 24) +
		(uint64(hashed[12]) << 32) +
		(uint64(hashed[13]) << 40) +
		(uint64(hashed[14]) << 48) +
		(uint64(hashed[15]) << 56)

	r := rand.New(rand.NewSource(seed))
	r.Shuffle(256, func(i, j int) {
		m.key[i], m.key[j] = m.key[j], m.key[i]
	})

	return m
}

func (m *Mixer) VarUint(i uint64) uint64 {
	lz := uint(bits.LeadingZeros64(i))
	mask := uint64(0x7fffffffffffffff) >> lz
	return m.Uint(63-lz, i&mask) + mask + 1
}

func (m *Mixer) s0(x uint64) uint64 {
	const mask = uint64(0xff) << 0
	return (x & ^mask) | (uint64(m.key[uint8(x)]) << 0)
}

func (m *Mixer) s1(x uint64) uint64 {
	const mask = uint64(0xff) << 8
	return (x & ^mask) | (uint64(m.key[uint8(x>>8)]) << 8)
}

func (m *Mixer) s2(x uint64) uint64 {
	const mask = uint64(0xff) << 16
	return (x & ^mask) | (uint64(m.key[uint8(x>>16)]) << 16)
}

func (m *Mixer) s3(x uint64) uint64 {
	const mask = uint64(0xff) << 24
	return (x & ^mask) | (uint64(m.key[uint8(x>>24)]) << 24)
}

func (m *Mixer) s4(x uint64) uint64 {
	const mask = uint64(0xff) << 32
	return (x & ^mask) | (uint64(m.key[uint8(x>>32)]) << 32)
}

func (m *Mixer) s5(x uint64) uint64 {
	const mask = uint64(0xff) << 40
	return (x & ^mask) | (uint64(m.key[uint8(x>>40)]) << 40)
}

func (m *Mixer) s6(x uint64) uint64 {
	const mask = uint64(0xff) << 48
	return (x & ^mask) | (uint64(m.key[uint8(x>>48)]) << 48)
}

func (m *Mixer) s7(x uint64) uint64 {
	const mask = uint64(0xff) << 56
	return (x & ^mask) | (uint64(m.key[uint8(x>>56)]) << 56)
}

func (m *Mixer) Uint(bits uint, a uint64) uint64 {
	hibit := uint64(1) << (bits - 1)
	mask := (uint64(1) << bits) - 1
	blocks := bits / 8

	round := func() {
		if blocks >= 1 {
			a = m.s0(a)
		}
		if blocks >= 2 {
			a = m.s1(a)
		}
		if blocks >= 3 {
			a = m.s2(a)
		}
		if blocks >= 4 {
			a = m.s3(a)
		}
		if blocks >= 5 {
			a = m.s4(a)
		}
		if blocks >= 6 {
			a = m.s5(a)
		}
		if blocks >= 7 {
			a = m.s6(a)
		}
		if blocks >= 8 {
			a = m.s7(a)
		}
		a1 := (a << 1) & mask
		if a&hibit != 0 {
			a1++
		}
		a = a1
	}

	for i := 0; i < preXorRounds; i++ {
		round()
	}

	a ^= m.xor & mask

	for i := 0; i < postXorRounds; i++ {
		round()
	}
	return a
}

// For Rule 214R purposes, we treat the high 32 bits as the previous
// state and the low 32 bits as the current state.

func mix214r(cur, prev uint32) uint32 {

	// for purposes of computation, we break the 64 bit word
	// into 16 4-bit chunks; each 4 bit chunk needs a 1-bit margin
	// on each side (hence 6 bits).  That acts as an index into
	// a 64-entry 4-bit wide array

	// copy the top bit to the bottom and the bottom bit to the
	// top

	var result uint32

	var x uint8

	// nibble[7]
	x = uint8((cur>>(28-1))&0x1F + ((cur & 0x1) << 5))
	result |= uint32(rule214mixup[x]) << 28

	// nibble[6]
	x = uint8((cur >> (24 - 1)) & 0x3F)
	result |= uint32(rule214mixup[x]) << 24

	// nibble[5]
	x = uint8((cur >> (20 - 1)) & 0x3F)
	result |= uint32(rule214mixup[x]) << 20

	// nibble[4]
	x = uint8((cur >> (16 - 1)) & 0x3F)
	result |= uint32(rule214mixup[x]) << 16

	// nibble[3]
	x = uint8((cur >> (12 - 1)) & 0x3F)
	result |= uint32(rule214mixup[x]) << 12

	// nibble[2]
	x = uint8((cur >> (8 - 1)) & 0x3F)
	result |= uint32(rule214mixup[x]) << 8

	// nibble[1]
	x = uint8((cur >> (4 - 1)) & 0x3F)
	result |= uint32(rule214mixup[x]) << 4

	// nibble[0]
	x = uint8((cur<<1)&0x1E + ((cur >> 31) & 0x1))
	result |= uint32(rule214mixup[x]) << 24

	return result ^ prev
}

var rule214mixup [64]uint8

func init() {
	for combo := 0; combo < 64; combo++ {
		// combo is a particular 6-bit pattern of input
		// for which we wish to produce the interior 4-bit output
		var result uint8
		for j := uint(0); j < 4; j++ {
			switch (combo >> j) & 7 {
			case 7, 6, 4, 2, 1:
				result |= 1 << j
			}
		}
		rule214mixup[combo] = result
	}
}
