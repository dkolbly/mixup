package main

import (
	"fmt"
	"os"
	"strconv"
	
	"bitbucket.org/dkolbly/mixup"
)

func main() {
	args := os.Args[1:]

	base := 16
	mix := false

	str := func(x uint64) string {
		return strconv.FormatUint(x, base)
	}
	
	if len(args) > 0 && args[0] == "-m" {
		mix = true
		args = args[1:]
	}

	if len(args) > 0 && args[0] == "-a" {
		base = 36
		args = args[1:]
	}

	if len(args) > 0 && args[0] == "-d" {
		base = 10
		args = args[1:]
	}
	
	if len(args) < 2 {
		fmt.Fprintf(os.Stderr, usage, os.Args[0])
		os.Exit(1)
	}

	mixer := mixup.New(args[0])
	for _, value := range args[1:] {
		
		x, err := strconv.ParseUint(value, base, 64)
		if err != nil {
			fmt.Fprintf(os.Stderr, "invalid value %q (%s)\n", value, err)
			os.Exit(1)
		}

		if mix {
			y := mixer.Uint64(x)
			fmt.Fprintf(os.Stdout, "%s -> %s\n", str(x), str(y))
		} else {
			fmt.Fprintf(os.Stdout, "%s <- %s\n", str(mixer.UnUint64(x)), str(x))
		}
	}
}

const usage = `usage: %s [-m] [-a|-d] <secret> <value>...

Flags:
   -m    mix instead of unmixing
   -a    use base 36 ("alpha") instead of base 16
   -d    use base 10 ("decimal") instead of base 16

Examples:
   
   $ unmix -m -a foobar 1 2 3
   1 -> 3ntsr9nwqtuw
   2 -> 1alqc8d1qs4w1
   3 -> 38tyqu2dtsqqd

   $ unmix -a foobar 3ntsr9nwqtuw 1alqc8d1qs4w1
   1 <- 3ntsr9nwqtuw
   2 <- 1alqc8d1qs4w1

`
