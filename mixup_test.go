package mixup

import (
	"fmt"
	"testing"
)

func TestMixer(t *testing.T) {
	m := New("foo0")
	var foo0 [10]uint64

	for i := 0; i < 10; i++ {
		x := m.Uint64(uint64(i))
		y := m.UnUint64(x)
		fmt.Printf("%016x --> %016x --> %016x\n", i, x, y)
		if uint64(i) != y {
			t.Errorf("expected inverse to work, but got %016x --> %016x --> %016x\n", i, x, y)
		}
		foo0[i] = x
	}

	m = New("foo1")
	for i := 0; i < 10; i++ {
		x := m.Uint64(uint64(i))
		y := m.UnUint64(x)
		fmt.Printf("%016x --> %016x --> %016x\n", i, x, y)
		if uint64(i) != y {
			t.Errorf("expected inverse to work, but got %016x --> %016x --> %016x\n", i, x, y)
		}
		if x == foo0[i] {
			t.Errorf("expected different mixups, but got %016x --> %016x for both", i, x)
		}
	}
}
