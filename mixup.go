package mixup

import (
	"crypto/sha256"
	"crypto/cipher"
	"crypto/des"
	"encoding/binary"
)

type Mixer struct {
	c cipher.Block
}

func New(secret string) *Mixer {
	h := sha256.New()
	h.Write([]byte(secret))

	c, err := des.NewTripleDESCipher(h.Sum(nil)[:24])
	if err != nil {
		panic(err)
	}

	return &Mixer{
		c: c,
	}
}

func (m *Mixer) Uint64(a uint64) uint64 {
	var tmp [8]byte
	binary.BigEndian.PutUint64(tmp[:], a)
	m.c.Encrypt(tmp[:], tmp[:])
	return binary.BigEndian.Uint64(tmp[:])
}

func (m *Mixer) UnUint64(a uint64) uint64 {
	var tmp [8]byte
	binary.BigEndian.PutUint64(tmp[:], a)
	m.c.Decrypt(tmp[:], tmp[:])
	return binary.BigEndian.Uint64(tmp[:])
}
